# README #

Shopping Cart Challenge

## What is this repository for? ##

* Quick summary
* Version

## Try the Online Demo

[http://ec2-50-19-54-108.compute-1.amazonaws.com/#/](http://ec2-50-19-54-108.compute-1.amazonaws.com/#/)

## How do I get set up? ##

In order to have the project running you need:  

* Node.js (used v10.16.13)  
* NPM (used v6.9.0)  
* MongoDb (used v4.4)  
* Java8 (used oracle jdk 1.8.0.144)  

### To run: ###
`mvn spring-boot:run`  
  
### To build: ###
`mvn clean package`  

### To execute built version: ###
`java -jar <package-name.verson>.jar`  

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

* Repo owner or admin