package com.ethoca.shopping.store;


import static org.assertj.core.api.Assertions.assertThat;

import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.ethoca.shopping.store.controller.ProductsController;
import com.ethoca.shopping.store.model.transfer.ProductDTO;

@SpringBootTest(webEnvironment =  WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class SmokeTest {

	@Autowired
	private ProductsController controller;
	
	@LocalServerPort
	private int port;
	
    @Autowired
    private WebTestClient webClient;
	
	@Test
	public void contextLoads() {
		assertThat(controller).isNotNull();
	}
	
	@Test
	public void testHttp() {
		webClient.get()
	        .uri("http://localhost:{port}/store/product/{productId}",port,2)
	        .accept(MediaType.APPLICATION_JSON)
	        .exchange()
            .expectStatus().isOk()
            .expectBody(ProductDTO.class)
	        .value(p -> p.getTitle(), IsEqual.equalTo("Apple iPhone 11 128GB"));
	}

}
