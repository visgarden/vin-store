package com.ethoca.shopping.store.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class ProductRepositoryTest {

	@MockBean
	private ProductRepository productRepository;
	
	@Test
	public void testSave() {
		productRepository.save(null);
	}
	
	@Test
	public void testFetch() {
		productRepository.findById(1);
	}

}
