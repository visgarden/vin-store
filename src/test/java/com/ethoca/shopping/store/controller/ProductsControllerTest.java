package com.ethoca.shopping.store.controller;

import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.ethoca.shopping.store.model.entity.Product;
import com.ethoca.shopping.store.model.transfer.ProductDTO;
import com.ethoca.shopping.store.model.transfer.StoreItemDTO;
import com.ethoca.shopping.store.service.ProductsService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@WebFluxTest(ProductsController.class)
@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
public class ProductsControllerTest {

    @Autowired
    private WebTestClient webClient;
    
    @MockBean
    private ProductsService productsService;

    private ModelMapper modelMapper;
    
    @BeforeEach
    void setUp(){
        modelMapper = new ModelMapper();
    }
    
    @Test
    public void testGetInventory() {
    	Product p1 = Product.create("Product A", "Description A", 1.99d, Collections.<String>emptyList());
    	Product p2 = Product.create("Product B", "Description B", 2.99d, Collections.<String>emptyList());
    	Product p3 = Product.create("Product C", "Description C", 3.99d, Collections.<String>emptyList());
    	StoreItemDTO item1 = new StoreItemDTO(5, modelMapper.map(p1, ProductDTO.class));
    	StoreItemDTO item2 = new StoreItemDTO(10, modelMapper.map(p2, ProductDTO.class));
    	StoreItemDTO item3 = new StoreItemDTO(15, modelMapper.map(p3, ProductDTO.class));
    	Flux<StoreItemDTO> flux = Flux.just(item1, item2, item3);
    	
    	when(productsService.getProductsInventory()).thenReturn(flux);
    	
		webClient.get()
	        .uri("/store/inventory")
	        .accept(MediaType.APPLICATION_JSON)
	        .exchange()
	        .expectStatus().isOk()
	        .expectBodyList(ProductDTO.class);
		
		Mockito.verify(productsService, Mockito.times(1)).getProductsInventory();
    }
    
    @Test
    public void testAddProduct() {
    	Product p1 = Product.create("Product A", "Description A", 1.99d, Collections.<String>emptyList());
    	p1.setId(1);
    	ProductDTO dto1 = modelMapper.map(p1, ProductDTO.class);
    	Mono<ProductDTO> mono = Mono.just(dto1);
    	
    	when(productsService.addProduct(dto1)).thenReturn(mono);
    	
    	webClient.post()
    		.uri("/store/product")
    		.contentType(MediaType.APPLICATION_JSON)
    		.accept(MediaType.APPLICATION_JSON)
    		.body(BodyInserters.fromValue(dto1))
	        .exchange()
            .expectStatus().isOk()
            .expectBody(ProductDTO.class)
            .value(c -> Assertions.assertEquals(c.getId(), 1));
            
    	Mockito.verify(productsService, Mockito.times(1)).addProduct(dto1);
       
    }
    
    @Test
    public void testGetproducts() {
    	Product p1 = Product.create("Product A", "Description A", 1.99d, Collections.<String>emptyList());
    	Product p2 = Product.create("Product B", "Description B", 2.99d, Collections.<String>emptyList());
    	Product p3 = Product.create("Product C", "Description C", 3.99d, Collections.<String>emptyList());
    	ProductDTO dto1 = modelMapper.map(p1, ProductDTO.class);
    	ProductDTO dto2 = modelMapper.map(p2, ProductDTO.class);
    	ProductDTO dto3 = modelMapper.map(p3, ProductDTO.class);
    	Flux<ProductDTO> flux = Flux.just(dto1, dto2, dto3);
    	
    	when(productsService.listAllProducts()).thenReturn(flux);
    	
		webClient.get()
        .uri("/store/product")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBodyList(ProductDTO.class);
		
		Mockito.verify(productsService, Mockito.times(1)).listAllProducts();
		
    }
    
    @Test
    public void testGetproduct() {
    	Product p1 = Product.create("Product A", "Description A", 1.99d, Collections.<String>emptyList());
    	p1.setId(1);
    	ProductDTO dto1 = modelMapper.map(p1, ProductDTO.class);
    	Mono<ProductDTO> mono = Mono.just(dto1);
    	
    	when(productsService.getProduct(1)).thenReturn(mono);
    	
		webClient.get()
        .uri("/store/product/{productId}", 1)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody(ProductDTO.class)
        .value(c -> Assertions.assertEquals(c.getId(), 1));
		
		Mockito.verify(productsService, Mockito.times(1)).getProduct(1);
    }
    
}
