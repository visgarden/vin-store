package com.ethoca.shopping.store.controller;

import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.ethoca.shopping.store.config.AppConfiguration;
import com.ethoca.shopping.store.model.entity.Customer;
import com.ethoca.shopping.store.model.entity.Product;
import com.ethoca.shopping.store.model.entity.StoreItem;
import com.ethoca.shopping.store.model.transfer.CustomerDTO;
import com.ethoca.shopping.store.service.CustomerService;

import reactor.core.publisher.Mono;

@WebFluxTest(CustomerController.class)
@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
public class CustomerControllerTest {

    @Autowired
    private WebTestClient webClient;
    
    @MockBean
    private CustomerService customerService;
    
    @MockBean
    private AppConfiguration security;

    private ModelMapper modelMapper;
    
    @BeforeEach
    void setUp(){
        modelMapper = new ModelMapper();
    }
	
    @Test
    public void testGetUserById() {
    	Customer c = new Customer("guest", 1);
    	Product p1 = Product.create("Product A", "Description A", 1.99d, Collections.<String>emptyList());
    	p1.setId(1);
    	c.getCart().getItems().add(new StoreItem(5, p1));
    	CustomerDTO dto1 = modelMapper.map(c, CustomerDTO.class);
    	Mono<CustomerDTO> mono = Mono.just(dto1);
    	
    	when(customerService.findUserById(1)).thenReturn(mono);
    	when(security.getLoggedUser()).thenReturn(1);
    	
		webClient.get()
        .uri("/store/customer")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody(CustomerDTO.class)
        .value(result -> Assertions.assertEquals(result.getName(), "guest"));
		
		Mockito.verify(customerService, Mockito.times(1)).findUserById(1);
		Mockito.verify(security, Mockito.times(1)).getLoggedUser();
    }
    
}
