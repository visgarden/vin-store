package com.ethoca.shopping.store.controller;

import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.ethoca.shopping.store.model.entity.Customer;
import com.ethoca.shopping.store.model.entity.Product;
import com.ethoca.shopping.store.model.entity.StoreItem;
import com.ethoca.shopping.store.model.transfer.CustomerDTO;
import com.ethoca.shopping.store.service.ShoppingCartService;

import reactor.core.publisher.Mono;

@WebFluxTest(ShoppingCartController.class)
@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
public class ShoppingCartControllerTest {

    @Autowired
    private WebTestClient webClient;
	
	@MockBean
	private ShoppingCartService shoppingCartService;
	
    private ModelMapper modelMapper;
    
    @BeforeEach
    void setUp(){
        modelMapper = new ModelMapper();
    }
	
	
	@Test
	public void testCheckoutCart() {
    	Customer c = new Customer("guest", 1);
    	CustomerDTO dto1 = modelMapper.map(c, CustomerDTO.class);
    	Mono<CustomerDTO> mono = Mono.just(dto1);
    	
    	when(shoppingCartService.checkoutCart()).thenReturn(mono);
    	
		webClient.get()
        .uri("/store/cart/checkout")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody(CustomerDTO.class)
        .value(result -> Assertions.assertTrue(result.getCart().getItems().isEmpty()));
    	
		Mockito.verify(shoppingCartService, Mockito.times(1)).checkoutCart();
	}
	
	@Test
	public void testSaveCart() {
    	Customer c = new Customer("guest", 1);
    	Product p1 = Product.create("Product A", "Description A", 1.99d, Collections.<String>emptyList());
    	p1.setId(1);
    	c.getCart().getItems().add(new StoreItem(5, p1));
    	CustomerDTO dto1 = modelMapper.map(c, CustomerDTO.class);
    	Mono<CustomerDTO> mono = Mono.just(dto1);
		
    	when(shoppingCartService.saveCart(dto1.getCart())).thenReturn(mono);
    	
    	webClient.post()
		.uri("/store/cart/save")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.body(BodyInserters.fromValue(dto1.getCart()))
        .exchange()
        .expectStatus().isOk()
        .expectBody(CustomerDTO.class)
        .value(result -> Assertions.assertEquals(result.getCart().getItems().size(), 1));
    	
    	Mockito.verify(shoppingCartService, Mockito.times(1)).saveCart(dto1.getCart());
	}
	
}
