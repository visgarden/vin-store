package com.ethoca.shopping.store.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class ShoppingCartServiceTest {

	@MockBean
	private ShoppingCartService shoppingCartService;
	
	@Test
	public void toDo() {
		shoppingCartService.checkoutCart();
	}
	
}
