import React, { Component } from 'react';
import { connect } from "react-redux";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import { IInventory } from '../models/IInventory';
import { IStoreItem } from '../models/IStoreItem';
import Product from './Product';
import { LOAD_STORE_URL } from "../const/endPointConstants";
import { STORE_LOADED, ADD_PRODUCT } from "../redux/actions/actionTypes"

interface IProps {
  inventory: IInventory,
  loadProducts: (result) => Function
  addProductToCart: (storeItem) => Function
}

class ProductsGrid extends Component<IProps> {

  public constructor(props: IProps) {
    super(props);
  }

  componentDidMount() {
    this.loadStoreInventory();
  }

  loadStoreInventory() {
    fetch(LOAD_STORE_URL)
      .then(res => res.json())
      .then((result) => {
        this.props.loadProducts({ items: result });
      });
  }

  render() {
    return (
      <Container fluid="md">
        <Row>
          {this.props.inventory.items.map((item: IStoreItem, index: number): JSX.Element => {
            return (
              <Product
                key={index}
                onAddToCart={this.props.addProductToCart}
                item={item} />)
          })}
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const { storeData } = state;
  return { ...storeData }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadProducts: (inventory) => dispatch({ type: STORE_LOADED, inventory }),
    addProductToCart: (storeItem) => dispatch({ type: ADD_PRODUCT, ...storeItem })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsGrid);