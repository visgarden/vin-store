import Header from './Header';
import ProductsGrid from './ProductsGrid';

const Home = (): JSX.Element => {
    return (
        <>
            <Header />
            <ProductsGrid />
        </>
    )
};
export default Home;