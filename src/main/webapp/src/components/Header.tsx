import * as React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';

const Header = (): JSX.Element => {
    return (
        <>
            <Jumbotron className="md-10">
                <Container>
                    <h1 className="display-4">Welcome !</h1>
                    <p className="lead">
                        Where you find the cheapest electronic products in the market.
		            </p>
                    <hr className="my-4" />
                </Container>
            </Jumbotron>
        </>
    );
};
export default Header;