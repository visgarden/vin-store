import React, { Component } from 'react';
import 'holderjs';
import {
    Button,
    Form,
    Card,
    Carousel,
    Image
} from 'react-bootstrap';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { IStoreItem } from '../models/IStoreItem';

interface IProps {
    item: IStoreItem,
    onAddToCart: (storeItem) => Function
  }

class Product extends Component<IProps,{qty}>{

    public constructor(props: IProps) {
        super(props);
        this.state = {
            qty : 1
        };
    }
    
    resetQty = () => {
        this.setState({qty : 1});
    }

    onQtyChange = (e) => {
        let qty = Number.parseInt(e.target.value);
        if(Number.isNaN(qty) || qty < 1){
            qty = 1;
        }
        this.setState({qty});
        return qty;           
        
    }

    onAddToCartHandle = (item : IStoreItem) => {
        const n = Number.isNaN(this.state.qty) ? 1 : this.state.qty;
        this.props.onAddToCart({item, n});
        this.resetQty();
    }

    render(){
        const {item, onAddToCart} = this.props;
        return (
            <div className="col-md-4 mb-5">
                <Card>
                    <Carousel>
                        {
                            item.product.pictures.map((picture, index): JSX.Element => {
                                return (
                                    <Carousel.Item key={index} interval={((Math.random() * 10000) + 5000)}>
                                        <Image className="d-block w-100" src={picture} alt="visual" />
                                    </Carousel.Item>
                                )
                            })
                        }
                    </Carousel>
                    <Card.Body>
                        <Card.Title>{item.product.title}</Card.Title>
                        <Card.Text>
                            {item.product.description}
                        </Card.Text>
                        <Form.Control onChange={this.onQtyChange} value={this.state.qty} type="number" placeholder="Qty" />
                        <Button onClick={() => this.onAddToCartHandle(item)} variant="outline-primary" block>
                            <FontAwesomeIcon icon={faCartPlus} /> Add to Cart
                        </Button>
                    </Card.Body>
                    <Card.Footer>
                        <small className="text-muted">Price : $ {item.product.price}</small>
                        <small className="text-muted float-right">| {item.total} |</small>
                    </Card.Footer>
                </Card>
            </div>
        );
    }
}

export default Product;