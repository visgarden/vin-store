import React, { Component } from 'react';
import { connect } from "react-redux";
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import Navigation from './Navigation';
import { ICustomer } from '../models/ICustomer';
import { LOAD_CUSTOMER_URL } from "../const/endPointConstants";
import { CUSTOMER_LOADED } from "../redux/actions/actionTypes"
import Home from "./Home";
import ShoppingCart from "./ShoppingCart";
import "./App.css";

interface IProps {
	customer: ICustomer
	loadCustomer: (result) => Function
}

class App extends Component<IProps> {

	public constructor(props: IProps) {
		super(props);
	}

	componentDidMount() {
		this.loadStoreCustomer();
	}

	loadStoreCustomer() {
		fetch(LOAD_CUSTOMER_URL)
			.then(res => res.json())
			.then((result) => {
				this.props.loadCustomer(result);
			});
	}

	render() {
		return (
			<>
				<Router>
					<Navigation {...this.props.customer} />
					<Switch>
						<Route exact path={["/","/home"]}  >
							<Home />
						</Route>
						<Route path="/cart">
							<ShoppingCart />
						</Route>
					</Switch>
				</Router>
			</>
		);
	}
}

const mapStateToProps = (state) => {
	const { customerData } = state;
	return { ...customerData };
}


const mapDispatchToProps = (dispatch) => {
	return {
		loadCustomer: (customer) => dispatch({ type: CUSTOMER_LOADED, customer })
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
