import * as React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBoxOpen } from '@fortawesome/free-solid-svg-icons'
import { ICustomer } from '../models/ICustomer';
import Button from 'react-bootstrap/Button';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'

const Navigation = (customer: ICustomer): JSX.Element => {
    return (
        <Navbar bg='light' fixed="top">
            <Navbar.Brand href="#home"><FontAwesomeIcon icon={faBoxOpen} /> Vin Store</Navbar.Brand>
            <Button variant="primary" href="#cart">
                <FontAwesomeIcon icon={faShoppingCart} /> My Cart {`(${customer.cart.items.length})`}
            </Button>
            <Navbar.Toggle />
            <Navbar.Collapse className="justify-content-end">
                <Navbar.Text>
                    Signed in as: <a href="#login">{customer.name}</a>
                </Navbar.Text>
            </Navbar.Collapse>
        </Navbar>
    );
};
export default Navigation;