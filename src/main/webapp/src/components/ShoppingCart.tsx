import { IShoppingCart } from "../models/IShoppingCart";
import { connect } from "react-redux";
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faMinus } from '@fortawesome/free-solid-svg-icons'
import { REMOVE_PRODUCT, ADD_PRODUCT, CHECKOUT_CART } from "../redux/actions/actionTypes"

import React, { Component } from 'react';
import Checkout from "./Checkout";

interface IProps {
  cart: IShoppingCart,
  add: (item) => Function,
  remove: (item) => Function,
  checkout: () => Function
}

class ShoppingCart extends Component<IProps> {

  public constructor(props: IProps) {
    super(props)
  }

  checkoutHandle = () => {
    
  }

  render() {
    const total : number = this.props.cart.items.length > 0 ?
     this.props
      .cart
      .items
      .map(p => p.product.price * p.total)
      .reduce((total, current) => total+current) : 0.0;
    return (
      <Container fluid="md">
        <h3>Products</h3>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Quantity</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.cart.items.map((item, index): JSX.Element => {
                return (
                  <tr key={index}>
                    <td>{item.product.id}</td>
                    <td>{item.product.title}</td>
                    <td>
                      <Row className="justify-content-around">
                        <Col xs lg="2">
                          <Button onClick={(e) => this.props.remove(item)} variant="outline-primary" size="sm">
                            <FontAwesomeIcon icon={faMinus} />
                          </Button>
                        </Col>
                        <Col md="auto">
                          <b>{item.total}</b>
                        </Col>
                        <Col xs lg="2">
                          <Button onClick={(e) => this.props.add(item)} variant="outline-primary" size="sm">
                            <FontAwesomeIcon icon={faPlus} />
                          </Button>
                        </Col>
                      </Row>
                    </td>
                    <td>$ {(item.product.price * item.total).toFixed(2)}</td>
                  </tr>
                )
              })
            }
            <tr>
              <td colSpan={4} >
                <h5 className="float-right">Total : $ {total.toFixed(2)}</h5>
              </td>
            </tr>
          </tbody>
        </Table>
        <Checkout enabled={total > 0} onCheckout={this.props.checkout}/>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const { customerData } = state;
  return { cart: {...customerData.customer.cart}}
}

const mapDispatchToProps = (dispatch) => {
  return {
    add: (item) => dispatch({type : ADD_PRODUCT, item, n : 1}),
    remove: (item) => dispatch({type : REMOVE_PRODUCT, item}),
    checkout: () => dispatch({type : CHECKOUT_CART})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);