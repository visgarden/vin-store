import * as React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

const Checkout = (props) : JSX.Element => {
    const [show, setShow] = React.useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
  
    const handleCheckout = () => {
        props.onCheckout();
        handleShow();
    }

    return (
      <>
        <Button variant="primary" className="float-right" onClick={handleCheckout} disabled={!props.enabled}>Checkout</Button>
          <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Order placed</Modal.Title>
          </Modal.Header>
          <Modal.Body>Woohoo, your order was successfully placed !</Modal.Body>
          <Modal.Footer>
            <Button href="#home" variant="primary" onClick={handleClose}>
              Go Back Shopping
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};
export default Checkout;