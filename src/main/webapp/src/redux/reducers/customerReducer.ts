import { ICustomerState } from "../../models/state/ICustomerState";
import { AnyAction } from "redux";
import {CUSTOMER_LOADED, ADD_PRODUCT, REMOVE_PRODUCT, CHECKOUT_CART} from "../actions/actionTypes";
import { SAVE_CART_URL,CHECKOUT_CART_URL } from "../../const/endPointConstants";
import { IShoppingCart } from "../../models/IShoppingCart";

export const INITIAL_STATE : ICustomerState = {
    customer : {
        name : "Guest",
        cart : {items : []}
    }
};

const customerLoaded = (state : ICustomerState, action : AnyAction) : ICustomerState => {
    return {
        ...state,
        customer : action.customer
    }
};

const checkoutCart = (state : ICustomerState, action : AnyAction) : ICustomerState => {
    const customer = {...state.customer};
    customer.cart = {items : []};
    processCheckoutCart();
    return {
        ...state,
        customer : customer
    }
};

const addItemToCart = (state : ICustomerState, action : AnyAction) : ICustomerState => {
    const customer = {...state.customer};
    const item = customer.cart.items.find(p => {
        return p.product.id === action.item.product.id
    })
    if(!item){
        customer.cart.items.push(
            {
                product : action.item.product,
                total : action.n
            }
        )
    }else{
        item.total += Math.max(action.n, 1);
    }
    saveCart(customer.cart);
    return {
        ...state,
        customer : customer
    }
};

const removeItemToCart = (state : ICustomerState, action : AnyAction) : ICustomerState => {
    const customer = {...state.customer}
    const item = customer.cart.items.find(p => {
        return p.product.id === action.item.product.id
    })
    if(item){
        if(item.total <= 1){
            customer.cart.items = customer.cart.items.filter(p => p.product.id !== item.product.id);
        }else{
            item.total--;
        }
    }else{
        //weird, shouldnt remove what doesn't exist
    }
    saveCart(customer.cart);
    return {
        ...state,
        customer : customer
    }
};

const saveCart = (cart : IShoppingCart) => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(cart)
    };

    fetch(SAVE_CART_URL, requestOptions)
      .then(res => res.json())
      .then((result) => {
        //dispach an event to update cart with server response
      });
}

const processCheckoutCart = () => {
    fetch(CHECKOUT_CART_URL)
    .then(res => res.json())
    .then((result) => {
      //dispach an event to update cart with server response
    });
}

const customerReducer = (state = INITIAL_STATE, action: AnyAction): ICustomerState => {
    switch (action.type) {
        case CUSTOMER_LOADED:
            return customerLoaded(state,action);
        case ADD_PRODUCT:
            return addItemToCart(state,action);
        case REMOVE_PRODUCT:
            return removeItemToCart(state,action);
        case CHECKOUT_CART:
            return checkoutCart(state,action)
        default:
            return state;
    }
}

export default customerReducer;