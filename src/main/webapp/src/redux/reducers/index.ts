import { combineReducers } from "redux";
import customerReducer from "./customerReducer";
import storeReducer from "./storeReducer";

const rootReducer = combineReducers<any>({
    customerData : customerReducer,
    storeData : storeReducer
});

export default rootReducer;