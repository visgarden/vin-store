import { IProduct } from "../../models/IProduct";
import { IStoreState } from "../../models/state/IStoreState";
import { AnyAction } from "redux";
import {STORE_LOADED} from "../actions/actionTypes"

export const MOCK_PRODUCT : IProduct = {
    id : 1,
    title : "Product 1",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.",
    price: 4.99,
    pictures: ["holder.js/400x300"]
};

export const INITIAL_STATE : IStoreState = {
    inventory : {
        items : [
            {total : 1, product : MOCK_PRODUCT}
        ]
    }
};

const storeLoaded = (state : IStoreState, action : AnyAction) : IStoreState => {
    return {
        ...state,
        inventory : action.inventory
    }
};

const storeReducer = (state = INITIAL_STATE, action: AnyAction): IStoreState => {
    switch (action.type) {
        case STORE_LOADED:
            return storeLoaded(state,action);
        default:
            return state;
    }
}

export default storeReducer;