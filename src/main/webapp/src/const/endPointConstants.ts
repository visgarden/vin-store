
const getHost = () : string => {
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        return "http://localhost:4080/";
    } else {
        return "/";
    }
}

const HOST = getHost();

export const LOAD_STORE_URL =  HOST + "store/inventory";
export const LOAD_CUSTOMER_URL = HOST + "store/customer";
export const SAVE_CART_URL = HOST + "store/cart/save";
export const CHECKOUT_CART_URL = HOST + "store/cart/checkout";