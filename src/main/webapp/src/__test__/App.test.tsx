import React from 'react';
import { render, screen } from '@testing-library/react';
import App from '../components/App';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import {HashRouter} from 'react-router-dom';
import * as customerReducer from "../redux/reducers/customerReducer";
import * as storeReducer from "../redux/reducers/storeReducer";

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const store = mockStore({ 
  customerData: {...customerReducer.INITIAL_STATE},
  storeData : {...storeReducer.INITIAL_STATE} 
})

test('Render App', () => {
  render(
    <Provider store={store}>
      <HashRouter>
        <App />
      </HashRouter>
    </Provider>
  );
  const welcome = screen.getByText(/Lorem ipsum dolor sit amet/i);
  expect(welcome).toBeInTheDocument();
});
