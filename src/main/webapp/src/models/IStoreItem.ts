import { IProduct } from "./IProduct";

export interface IStoreItem{
    total: number,
    product : IProduct
}