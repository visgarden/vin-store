import { IStoreItem } from "./IStoreItem";

export interface IInventory{
    items : Array<IStoreItem>
}