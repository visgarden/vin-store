import { IStoreItem } from "./IStoreItem";

export interface IShoppingCart{
    items : Array<IStoreItem>
}