import { IInventory } from "../IInventory";

export interface IStoreState{
    inventory : IInventory;
}