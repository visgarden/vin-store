import { IShoppingCart } from "./IShoppingCart";

export interface ICustomer{
    name : string,
    cart : IShoppingCart,
    invoces? : any
}