package com.ethoca.shopping.store.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

	public int getLoggedUser() {
		return 1;
	}
    
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
	
}
