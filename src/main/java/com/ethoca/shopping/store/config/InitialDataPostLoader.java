package com.ethoca.shopping.store.config;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

import com.ethoca.shopping.store.model.entity.Customer;
import com.ethoca.shopping.store.model.entity.Product;
import com.ethoca.shopping.store.repository.CustomerRepository;
import com.ethoca.shopping.store.repository.ProductRepository;

import reactor.core.publisher.Flux;

@Configuration
public class InitialDataPostLoader implements ApplicationListener<ContextRefreshedEvent> {
	
	public static final Object[][] DATA = {  
		      {"Apple MacBook Pro 13.3\"", "The MacBook Pro elevates the notebook to a whole new level of performance and portability. Wherever your ideas take you, you’ll get there faster than ever with high‑performance processors and memory, advanced graphics, blazing fast storage and more -- all in a compact 1.4-kg package. The Touch Bar puts powerful shortcuts front and centre, and Touch ID provides fast authentication", 154.99,Arrays.asList( 
			    	  	"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/146/14627/14627724.jpg",
			    	  	"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/146/14627/14627724_1.jpg",
			    	  	"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/146/14627/14627724_2.jpg",
			    	  	"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/146/14627/14627724_3.jpg")},  
			      {"Apple iPhone 11 128GB", "A new dual‑camera system captures more of what you see and love. The fastest chip ever in a smartphone and all day battery life let you do more and charge less. And the highest‑quality video in a smartphone, so your memories look better than ever.", 145.99,Arrays.asList( 
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/138/13897/13897388.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/138/13897/13897388_2.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/138/13897/13897388_4.jpg")},  
			      {"Amazon Echo Dot (4th Gen) Smart Speaker", "Perfect on your nightstand or an end table, or to round out any room, the Amazon Echo Dot (4th Gen) features a clock and Amazon Alexa. This smart speaker delivers clear, crisp vocals and balanced bass, while its sleek, compact design fits perfectly into small spaces. Use voice commands to ask Alexa to play music, answer questions, give you the news and weather, set alarms, and much more.", 7.99,Arrays.asList( 
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/149/14966/14966500.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/500x500/149/14966/14966500_1.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/149/14966/14966500_2.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/149/14966/14966500_3.jpg")},
			      {"Amazon Kindle Paperwhite 32GB 6\"", "No matter where your day takes you, the Amazon Kindle Paperwhite eBook reader lets you bring your library along for the ride. Boasting a 6\" touchscreen display with E-ink technology, this ultra-light, ultra-thin eReader provides a glare-free reading experience in any light. Its long-life battery keeps the device powered up for weeks.", 16.9,Arrays.asList( 
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/134/13496/13496912.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/134/13496/13496912_2.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/134/13496/13496912_3.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/134/13496/13496912_4.jpg")},
			      {"Google Pixel 5 128GB", "Take your smartphone to the limits with the Google Pixel 5. This ultimate 5G Google phone is powered by a Qualcomm Snapdragon 765G processor, 8GB of RAM, and Android 11 for smooth streaming, gaming, and more. With stunning photo and video capabilities, wireless charging and water-resistance, it's an amazing tool. The all-day battery can last up to 48 hours with Extreme Battery Saver.", 79.9,Arrays.asList( 
			    		"https://multimedia.bbycastatic.ca/multimedia/products/500x500/149/14961/14961489.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/500x500/149/14961/14961489_1.jpg")},
			      {"Google Chromecast with Google TV", "Stream the entertainment you love to your TV in up to 4K resolution with the Google Chromecast with Google TV. This media streamer works with almost any smart TV with an HDMI port and Wi-Fi. Google TV and Android apps give you access to movies, TV shows, music, and much more. Cast from your compatible device, and use the voice remote for hands-free search and suggestions.", 6.99,Arrays.asList(
			    		"https://multimedia.bbycastatic.ca/multimedia/products/500x500/149/14924/14924711.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/500x500/149/14924/14924711_1.jpg")},
			      {"Bose Noise Cancelling Bluetooth Headphones 700", "Get astonishing wireless audio at work or home with the Bose Noise Cancelling Headphones 700. Send messages, search for information, and more with voice commands via Google Assistant or Amazon Alexa. The 4-microphone system hears your commands with accuracy and makes your voice sound crystal-clear on calls.", 47.90,Arrays.asList( 
			    		"https://multimedia.bbycastatic.ca/multimedia/products/500x500/135/13595/13595376.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/500x500/135/13595/13595376_3.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/500x500/135/13595/13595376_6.jpg",
			    		"https://multimedia.bbycastatic.ca/multimedia/products/1500x1500/135/13595/13595376_7.jpg")}
			  };
	
	private Logger logger = LogManager.getLogger(this.getClass());
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		logger.info(String.format("() {}", "System Metadata initialization"));
		createDefaulData();
	}

	private void createDefaulData( ) {
		  customerRepository
		  	.deleteAll()
		  	.thenReturn(new Customer("Ethoca", 1))
		  	.flatMap(customerRepository::save)
			.thenMany(productRepository.findAll())
			.subscribe(customer -> System.out.println("Saving " + customer.toString()));
		  
			productRepository
				.deleteAll()
				.thenMany(
					Flux.just(DATA).map(productData -> {
					return Product.create(productData[0], productData[1], productData[2], productData[3]);
				})
				.flatMap(productRepository::save))
				.thenMany(productRepository.findAll())
				.subscribe(product -> System.out.println("Saving " + product.toString()));
	}
}