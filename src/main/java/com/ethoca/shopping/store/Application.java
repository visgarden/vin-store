package com.ethoca.shopping.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is a single monolithic shopping store application. But we could break
 * down the app in microservices such as: 
 * - Product Catalog Management 
 * - Inventory Management 
 * - Product Search 
 * - Order Management 
 * - Delivery Management 
 * - User Management
 * 
 * Communicating with each other through WebClient or a Bus (like kafka or rabbitmq)
 * 
 * @author vin
 *
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
