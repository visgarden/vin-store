package com.ethoca.shopping.store.service.helper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ethoca.shopping.store.model.entity.Customer;
import com.ethoca.shopping.store.model.entity.Product;
import com.ethoca.shopping.store.model.entity.ShoppingCart;
import com.ethoca.shopping.store.model.transfer.CustomerDTO;
import com.ethoca.shopping.store.model.transfer.ProductDTO;
import com.ethoca.shopping.store.model.transfer.ShoppingCartDTO;

@Service
public class DataTransferHelper {

    @Autowired
    private ModelMapper modelMapper;
	
	public CustomerDTO buildCustomer(Customer c) {
		return modelMapper.map(c, CustomerDTO.class);
	}
	
	public ProductDTO buildProduct(Product p) {
		return modelMapper.map(p, ProductDTO.class);
	}
	
	public Product buildProduct(ProductDTO p) {
		return modelMapper.map(p, Product.class);
	}
	
	public ShoppingCartDTO buildShoppingCart(ShoppingCart sc) {
		return modelMapper.map(sc, ShoppingCartDTO.class);
	}
	
	public ShoppingCart buildShoppingCart(ShoppingCartDTO sc) {
		return modelMapper.map(sc, ShoppingCart.class);
	}
	
}
