package com.ethoca.shopping.store.service;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.ethoca.shopping.store.model.entity.IdSequence;

import reactor.core.publisher.Mono;

@Service
public class SequenceGeneratorService  {
	
	 @Autowired 
	private ReactiveMongoOperations mongoOperations;
	
	private boolean initialized = false;
	 
	public Integer generateSequence(String seqName) {
	    Mono<IdSequence> counter = mongoOperations
	    		.findAndModify(query(where("_id").is(seqName)),
	    				new Update().inc("seq",1),
	    				options().returnNew(false).upsert(true),
	    				IdSequence.class);
	    if(!initialized) {
	    	counter.subscribe(id ->	{/*bogus - spring-data bug*/});
	    	initialized = true;
	    }
	    return counter.block().getSeq();
	}
	
}
