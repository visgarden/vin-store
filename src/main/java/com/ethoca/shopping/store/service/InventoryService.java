package com.ethoca.shopping.store.service;

import org.springframework.stereotype.Service;

import io.netty.util.internal.ThreadLocalRandom;
import reactor.core.publisher.Mono;

@Service
public class InventoryService {

	public Mono<Integer> getProductInventory(int productId){
		ThreadLocalRandom rdn = ThreadLocalRandom.current();
		int inventory = rdn.nextInt(1, 50);
		return Mono.just(inventory);
	}
	
}
