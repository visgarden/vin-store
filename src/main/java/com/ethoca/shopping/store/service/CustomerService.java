package com.ethoca.shopping.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ethoca.shopping.store.model.transfer.CustomerDTO;
import com.ethoca.shopping.store.repository.CustomerRepository;
import com.ethoca.shopping.store.service.helper.DataTransferHelper;

import reactor.core.publisher.Mono;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private DataTransferHelper dtoHelper;

	public Mono<CustomerDTO> findUserById(int loggedUser) {
		return customerRepository.findById(loggedUser).map(c -> dtoHelper.buildCustomer(c));
	}
	
}
