package com.ethoca.shopping.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ethoca.shopping.store.config.AppConfiguration;
import com.ethoca.shopping.store.model.entity.ShoppingCart;
import com.ethoca.shopping.store.model.transfer.CustomerDTO;
import com.ethoca.shopping.store.model.transfer.ShoppingCartDTO;
import com.ethoca.shopping.store.repository.CustomerRepository;
import com.ethoca.shopping.store.service.helper.DataTransferHelper;

import reactor.core.publisher.Mono;

@Service
public class ShoppingCartService {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private DataTransferHelper dtoHelper;
	
	@Autowired
	private AppConfiguration security;

	public Mono<CustomerDTO> checkoutCart() {
		return customerRepository.findById(security.getLoggedUser()).map(c -> {
			c.setCart(new ShoppingCart()); // TODO generate an Invoice
			return c;
		}).flatMap(customerRepository::save)
		.map(dtoHelper::buildCustomer);
	}

	public Mono<CustomerDTO> saveCart(ShoppingCartDTO cart) {
		return customerRepository.findById(security.getLoggedUser()).map(c -> {
			c.setCart(dtoHelper.buildShoppingCart(cart));
			return c;
		}).flatMap(customerRepository::save)
		.map(dtoHelper::buildCustomer);
	}
}
