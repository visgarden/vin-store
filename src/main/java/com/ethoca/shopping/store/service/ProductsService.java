package com.ethoca.shopping.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ethoca.shopping.store.model.entity.Product;
import com.ethoca.shopping.store.model.transfer.ProductDTO;
import com.ethoca.shopping.store.model.transfer.StoreItemDTO;
import com.ethoca.shopping.store.repository.ProductRepository;
import com.ethoca.shopping.store.service.helper.DataTransferHelper;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProductsService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private InventoryService inventoryService;

	@Autowired
	private DataTransferHelper dtoHelper;

	public Flux<StoreItemDTO> getProductsInventory() {
		Flux<Product> products = productRepository.findAll();
		Flux<StoreItemDTO> items = products.flatMap(p -> {
			Mono<Integer> qtyTask = inventoryService.getProductInventory(p.getId());
			Mono<ProductDTO> productTask = Mono.just(dtoHelper.buildProduct(p));
			return qtyTask.zipWith(productTask, (qty, product) -> {
				return new StoreItemDTO(qty, product);
			});
		});
		return items;
	}

	public Mono<ProductDTO> addProduct(ProductDTO product) {
		return productRepository
				.save(dtoHelper.buildProduct(product))
				.map(dtoHelper::buildProduct);
	}

	public Flux<ProductDTO> listAllProducts() {
		return productRepository
				.findAll()
				.map(dtoHelper::buildProduct);
	}

	public Mono<ProductDTO> getProduct(Integer productId) {
		return productRepository
				.findById(productId)
				.map(dtoHelper::buildProduct);
	}

}
