package com.ethoca.shopping.store.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ethoca.shopping.store.config.AppConfiguration;
import com.ethoca.shopping.store.model.transfer.CustomerDTO;
import com.ethoca.shopping.store.service.CustomerService;

import reactor.core.publisher.Mono;

@Controller
@CrossOrigin
@RequestMapping(path = "/store")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private AppConfiguration security;
	
    @GetMapping("/customer")
    public @ResponseBody Mono<CustomerDTO> getCustomer() {  
        return customerService.findUserById(security.getLoggedUser());  
    }
	
}
