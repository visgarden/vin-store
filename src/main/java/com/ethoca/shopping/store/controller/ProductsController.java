package com.ethoca.shopping.store.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ethoca.shopping.store.model.transfer.ProductDTO;
import com.ethoca.shopping.store.model.transfer.StoreItemDTO;
import com.ethoca.shopping.store.service.ProductsService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@CrossOrigin
@RequestMapping(path = "/store")
public class ProductsController {

	@Autowired
	private ProductsService productsService;
	  
    @GetMapping("/inventory")
    public @ResponseBody Flux<StoreItemDTO> getInventory() {
    	return productsService.getProductsInventory();
    }
    
    @PostMapping("/product")
    public @ResponseBody Mono<ProductDTO> upInsertProduct(@RequestBody ProductDTO product) {
        return productsService.addProduct(product);  
    }  
    
    @GetMapping("/product")
    public @ResponseBody Flux<ProductDTO> getProducts() {  
        return productsService.listAllProducts();
    }
    
    @GetMapping("/product/{productId}")
    public @ResponseBody Mono<ProductDTO> getProduct(@PathVariable Integer productId) {  
        return productsService.getProduct(productId);
    }
	
}
