package com.ethoca.shopping.store.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ethoca.shopping.store.model.transfer.CustomerDTO;
import com.ethoca.shopping.store.model.transfer.ShoppingCartDTO;
import com.ethoca.shopping.store.service.ShoppingCartService;

import reactor.core.publisher.Mono;

@Controller
@CrossOrigin
@RequestMapping(path = "/store/cart")
public class ShoppingCartController {

	@Autowired
	private ShoppingCartService shoppingCartService;
	
    @GetMapping("/checkout")
    public @ResponseBody Mono<CustomerDTO> checkout() {  
    	return shoppingCartService.checkoutCart();
    } 
	
    @PostMapping("/save")
    public @ResponseBody Mono<CustomerDTO> saveCart(@RequestBody ShoppingCartDTO cart) {  
    	return shoppingCartService.saveCart(cart);
    } 
	
}
