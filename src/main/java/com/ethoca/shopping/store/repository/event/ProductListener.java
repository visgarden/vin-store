package com.ethoca.shopping.store.repository.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import com.ethoca.shopping.store.model.entity.Product;
import com.ethoca.shopping.store.service.SequenceGeneratorService;

@Component
public class ProductListener extends AbstractMongoEventListener<Product>{

	@Autowired
	private SequenceGeneratorService sequenceGenerator;
	
	@Override
	public void onBeforeConvert(BeforeConvertEvent<Product> event) {
	    if (event.getSource().getId() == null || event.getSource().getId() < 0) {
	        event.getSource().setId(sequenceGenerator.generateSequence(Product.SEQUENCE_NAME));
	    }
	}
	
}
