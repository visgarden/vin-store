package com.ethoca.shopping.store.model.entity;

import java.util.Map;

/**
 * The store abstraction
 * @author vin
 *
 */
public class Store {

	private String 					name;
	private Map<Product, Integer> 	producs;
	
	public Store() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<Product, Integer> getProducs() {
		return producs;
	}

	public void setProducs(Map<Product, Integer> producs) {
		this.producs = producs;
	}
	
}
