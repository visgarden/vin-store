package com.ethoca.shopping.store.model.transfer;

public class StoreItemDTO {
	
	private int total;
	private ProductDTO product;
	
	public StoreItemDTO() {
	}
	
	public StoreItemDTO(int total, ProductDTO product) {
		this.total = total;
		this.product = product;
	}

	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public ProductDTO getProduct() {
		return product;
	}
	public void setProduct(ProductDTO product) {
		this.product = product;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + total;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StoreItemDTO other = (StoreItemDTO) obj;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (total != other.total)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StoreItemDTO [total=" + total + ", product=" + product + "]";
	}
	
}
