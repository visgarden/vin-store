package com.ethoca.shopping.store.model.transfer;

public class InventoryDTO {
	
	private StoreItemDTO items;

	public InventoryDTO() {
	}

	public InventoryDTO(StoreItemDTO items) {
		this.items = items;
	}

	public StoreItemDTO getItems() {
		return items;
	}

	public void setItems(StoreItemDTO items) {
		this.items = items;
	}

}
