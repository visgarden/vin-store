package com.ethoca.shopping.store.model.entity;

import java.util.LinkedList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * A given product an its attributes
 * @author vin
 *
 */
@Document(collection = "products")
public class Product {

    @Transient
    public static final String SEQUENCE_NAME = "products_sequence";
	
	@Id
	private Integer 		id;
	private Double 			price;
	private String 			title;
	private String 			description;
	private List<String>	pictures;

	/**
	 * Factory method to help creating new Products
	 * @param title type String
	 * @param description type String
	 * @param price type Double
	 * @param pictures type List<String>
	 * @return Product
	 */
	@SuppressWarnings("unchecked")
	public static Product create(Object title, Object description, Object price, Object pictures) {
		Product p = new Product();
		p.setTitle((String) title);
		p.setDescription((String) description);
		p.setPrice((Double) price);
		p.pictures = new LinkedList<String>();
		p.pictures.addAll((List<String>)pictures);
		return p;
	}
	
	public Product() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getPictures() {
		return pictures;
	}

	public void setPictures(List<String> pictures) {
		this.pictures = pictures;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", price=" + price + ", title=" + title + ", description=" + description
				+ ", pictures=" + pictures + "]";
	}
	
}
