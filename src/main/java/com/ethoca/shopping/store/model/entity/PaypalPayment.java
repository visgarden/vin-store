package com.ethoca.shopping.store.model.entity;

public class PaypalPayment extends Payment{

	@Override
	public PaymentType getType() {
		return PaymentType.PAYPAL;
	}
	
}
