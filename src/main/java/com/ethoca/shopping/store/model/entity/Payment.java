package com.ethoca.shopping.store.model.entity;

import java.util.Date;

/**
 * 
 * Payment abstraction
 * 
 * @author vin
 *
 */
public abstract class Payment {
	
	private Integer 		id;
	private Double 			value;
	private Date   			date;
	private PaymentStatus 	status;
	private String			transaction;
	
	public Payment() {
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public abstract PaymentType getType();

	public Double getValue() {
		return value;
	}
	
	public void setValue(Double value) {
		this.value = value;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	
}
