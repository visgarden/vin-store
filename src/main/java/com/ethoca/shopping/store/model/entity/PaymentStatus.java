package com.ethoca.shopping.store.model.entity;

public enum PaymentStatus {

	APPROVED,
	PENDING,
	CANCELED,
	REJECTED
	
	
}
