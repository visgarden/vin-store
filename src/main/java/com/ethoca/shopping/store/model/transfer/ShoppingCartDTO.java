package com.ethoca.shopping.store.model.transfer;

import java.util.List;

public class ShoppingCartDTO {
	
	private List<StoreItemDTO> items;

	public ShoppingCartDTO() {
	}

	public List<StoreItemDTO> getItems() {
		return items;
	}

	public void setItems(List<StoreItemDTO> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShoppingCartDTO other = (ShoppingCartDTO) obj;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ShoppingCartDTO [items=" + items + "]";
	}
	
}
