package com.ethoca.shopping.store.model.entity;

import java.util.List;

/**
 * Invoice abstraction
 * @author vin
 *
 */
public class Invoice {

	private Integer			id;
	private Order 			order;
	private List<Payment> 	payments;
	
	public Order getOrder() {
		return order;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	
	public List<Payment> getPayments() {
		return payments;
	}
	
	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}
	
}
