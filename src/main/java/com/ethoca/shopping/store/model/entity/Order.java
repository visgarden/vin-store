package com.ethoca.shopping.store.model.entity;

import java.util.Map;

/**
 * Client Order
 * @author vin
 *
 */
public class Order {

	private Integer 				id;
	private Map<Product, Integer> 	items;
	
	public Order() {
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Map<Product, Integer> getItems() {
		return items;
	}
	public void setItems(Map<Product, Integer> items) {
		this.items = items;
	}
	
}
