package com.ethoca.shopping.store.model.entity;

public class StoreItem {
	
	private Product product;
	private int		total;
	
	public StoreItem(int total, Product product) {
		this.product = product;
		this.total = total;
	}
	
	public StoreItem() {
	}

	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}

