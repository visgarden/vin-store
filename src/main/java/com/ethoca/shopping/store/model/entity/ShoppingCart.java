package com.ethoca.shopping.store.model.entity;

import java.util.LinkedList;
import java.util.List;

/**
 * Keep the products until checkout
 * @author vin
 *
 */
public class ShoppingCart {

	private List<StoreItem> 	items;
	
	public ShoppingCart() {
		items = new LinkedList<StoreItem>();
	}
	
	public List<StoreItem> getItems() {
		return items;
	}

	public void setItems(List<StoreItem> items) {
		this.items = items;
	}

	
	
}
