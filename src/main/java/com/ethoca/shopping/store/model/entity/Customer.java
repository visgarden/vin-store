package com.ethoca.shopping.store.model.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Customer abastraction
 * @author vin
 *
 */
@Document(collection = "customers")
public class Customer {

    @Id
    @JsonIgnore
	private Integer				id;
	private String 				name;
	private ShoppingCart 		cart;
	private List<Integer> 		invoices;
	
	public Customer(String name, Integer id) {
		this();
		this.name = name;
		this.id = id;
	}

	public Customer() {
		cart = new ShoppingCart();
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ShoppingCart getCart() {
		return cart;
	}

	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}

	public List<Integer> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Integer> invoices) {
		this.invoices = invoices;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", cart=" + cart + ", invoices=" + invoices
				+ "]";
	}
	
}
